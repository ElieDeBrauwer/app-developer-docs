# App UI URL

When testing an App with an appropriate app-specific App UI within the EATS, consider the following: 

* Your App UI must be reachable by the Workbench Service, from within the `workbench-service` container
* Your App UI must be built according to [Tutorial: Frontend](tutorial_frontend/tutorial)

Furthermore, due to the internal architecture of the EATS, there are additional steps to be taken. The following diagram illustrates the integration of the App UI into the Workbench Client 2.0:

<img src="images/app_test_suite/wbc2/app_ui_diag.png"></img>

As shown in the diagram the Workbench Service (WBS) runs as a docker container in the docker network *EATS*. To be able to reach your locally served App UI resources the WBS must contact the host system from within the docker network. In order to make this possible, it is necessary that:

* your locally served App UI is bound to host `0.0.0.0`
* your locally served App UI allows the host `host.docker.internal`

The following command serves as an example:

```bash
nx serve --host 0.0.0.0 --port 4200 --allowed-hosts host.docker.internal
```

For the example given in the diagram, the resulting **App UI URL** to use with `--app-ui-url` when registering your app would be: `http://host.docker.internal:4200`.
