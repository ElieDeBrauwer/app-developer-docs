# App with App UI

This section will guide you through using the EATS with the sample app from the [Tutorial: Frontend](tutorial_frontend/tutorial.md#), i.e. sample_frontend_app_01.

Afterwards you will have executed every step necessary to test your own App and App UI.

## Prepare App

First we need to build the docker image for the example app of the tutorial:

```bash
docker build -t sample_frontend_app_01 ./sample-apps/sample_apps/valid/with_frontend/sample_frontend_app_01/
```

## Register App

Next we need to register our app at our locally running Marketplace-Service. The command to register the example app requires the following parameters:

* the docker image name: `sample_frontend_app_01`
* the path to the ead: `ead.json`
* the URL to the App UI: `--app-ui-url http://host.docker.internal:8888/sample-app-ui`

EATS 2.0 ships the sample-app-ui using a container. Please note, that this is only for demonstration purposes. App UIs will not be published in the form of container images. For more information refer to the [publish](publish.md#) section.

The Sample App UI is served through an NGINX reverse proxy. The address on localhost is `http://localhost:8888/sample-app-ui`. When the WBC loads the App UI, it does not access it directly through the `localhost` address. Instead it requests the App UI resources through the Workbench v2 Frontends API. The Workbench Service has access to the host network via `host.docker.internal`. Therefore the address translates to `http://host.docker.internal:8888/sample-app-ui` for the `--app-ui-url` setting. For more information refer to the [App UI URL](eats/app_ui_url.md#) section.

!> If you are using a different port for the NGINX reverse proxy, the port in the URL must be adapted accordingly.

```bash
cd ./sample-apps/sample_apps/valid/with_frontend/sample_frontend_app_01/
eats apps register ead.json sample_frontend_app_01 --app-ui-url http://host.docker.internal:8888/sample-app-ui > app.env
```

?> If your app requires configuration parameters, `--config-file <path/to/configuration.json>` must be appended.

The above command will create the file `app.env` containing an `APP_ID`, which we can be stored as an environment variable for later use with:

```bash
export $(xargs <app.env)
echo $APP_ID
```

To view all registered apps use:

```bash
eats apps list
```

## Register and Run Job

A new job is registered by using the `APP_ID` and providing a folder containing the job inputs. This will produce the environment variables for your app (`EMPAIA_JOB_ID`, `EMPAIA_TOKEN`, `EMPAIA_APP_API`) that will be saved to `job.env`.

```bash
eats jobs register $APP_ID ./eats_inputs > ./job.env
```

?> When a job  is registered, all inputs from `./eats_inputs` (except WSIs) are created and persisted in the **Medical Data Service**. Implicitly a case, an examination and a scope are created as well. The ID of the created scope is used as the creator of all posted inputs. The creator of inputs is important for the [Workbench v1 Legacy Support](/eats/workbench_v1_legacy_support.md#)

Run the app, providing the file including the environment variables `job.env`:

```bash
eats jobs run ./job.env
```

?> A registered job can only be run once. Starting the job a second time will result in an error.

## Job Status

The job ID can be retrieved from the `job.env` file to be used in other commands.

```bash
export $(xargs <job.env)
echo $EMPAIA_JOB_ID
```

Regularly check the job's status until it is `COMPLETED`.

?> Depending on the elapsed time, the job will be in one of these states: `SCHEDULED`, `RUNNING` or `COMPLETED`. If the status is `ERROR`, the job did not complete successfully. See [Debugging](/eats/advanced.md#debugging) for help.

```bash
eats jobs status ${EMPAIA_JOB_ID}
```

## Results - JSON

To export the outputs of the jobs:

```bash
eats jobs export ${EMPAIA_JOB_ID} ./job-outputs
```

## Results - Workbench Client

Open http://localhost:8888/wbc2/ in a Browser to review the job results using the EMPAIA Workbench Client 2.0.

!> Up to step 3 the navigation is the same for all apps. From step 4 onward each App UI has its own workflow and the navigation is different. Below the navigation of the Sample App UI from the [Tutorial: Frontend](tutorial_frontend/tutorial.md#) is demonstrated.

For simplicity, all slides are added to a static single case. Navigate to:

1) **Cases** and select the case:

<img src="images/app_test_suite/wbc2/01_wbc2_case.png"></img>

2) **Examinations** and select the OPEN examination:

<img src="images/app_test_suite/wbc2/02_wbc2_exa.png"></img>

3) **Apps** and select your app:

<img src="images/app_test_suite/wbc2/03_wbc2_app.png"></img>

**App UI Specific Navigation**

4) **Slides** and select the slide:

<img src="images/app_test_suite/wbc2/04_wbc2_slide.png"></img>

5) View the results. E.g.

* The app took one ROI *REGION_OF_INTEREST* as input
  * The green check mark indicates a successful job execution
* The App generated three outputs referencing the input ROI
  * *number positive* as an integer value
  * *number negative* as an integer value
  * *positivity* as a float value

<img src="images/app_test_suite/wbc2/05_wbc2_results.png"></img>

6) Navigate in the viewer to see the point annotations generated by the app
 
<img src="images/app_test_suite/wbc2/06_wbc2_line_thick.png"></img>

7) Choose a different color palatte to increase contrast and visibility of annotations for different stains and tissue types. Additionally the stroke width (i.e. line thickness) of the annotations can be adjusted if needed.

<img src="images/app_test_suite/wbc2/07_wbc2_color.png"></img>

8) If multiple jobs were run you can hide all results or only specific job results (check box beside input ROI - not shown in picture)

<img src="images/app_test_suite/wbc2/08_wbc2_hide.png"></img>
