# Basic Usage

This section guides you through some remaining steps to set up your test environment and demonstrates basic commands to use the App Test Suite.

## Download Test Slide

Download a sample WSI from the [Openslide WSI repository](http://openslide.cs.cmu.edu/download/openslide-testdata/Aperio/CMU-1.svs) and store it at some path **/SOME/PATH/TO/WSIS/CMU-1.svs**.

## Download Sample Apps

Download or clone the [Sample Apps Repository](https://gitlab.com/empaia/integration/sample-apps), known from the tutorial:

```bash
git clone https://gitlab.com/empaia/integration/sample-apps.git
```

For all proceeding instructions we assume the following folder structure. Also, interesting sample apps for the Test Suite are shown in the folder structure:

```
eats/
├── .venv/
├── dist/
├── licenses/
└── sample-apps/
    └── sample_apps/
        └── valid/
            ├── others/
            │   ├── _12_eats_input_templates
            │   └── _13_eats_nested_large_collections
            ├── tutorial/
            │   ├── _01_simple_app/
            │   ├── _02_collections_and_references/
            │   ├── _03_annotation_results/
            │   ├── _04_output_references_output
            │   ├── _05_classes
            │   ├── _06_large_collections
            │   ├── _07_configuration/
            │   └── _08_error
            └── with_frontend
                └── sample_frontend_app_01
```

## Help Command

For information about the CLI usage and arguments execute:

```bash
eats --help
```

For more information on each sub command, e.g.:

```bash
eats jobs --help
eats jobs register --help
```

## Start services

First we need to prepare a file, that tells the eats services where to find our WSIs, i.e. the WSI we just downloaded to **/SOME/PATH/TO/WSIS/CMU-1.svs**:

```bash
code ./wsi-mount-points.json
```

?> The `code` command starts Visual Studio Code to edit the file, but you can use any other text editor instead.

```JSON
{
    "/SOME/PATH/TO/WSIS": "/data",
    "/SOME/OTHER/PATH/TO/WSIS": "/data2"
}
```

In the JSON object notation, the key represents the path on your local machine while the value is the mount point inside the service containers (namely `wsi-service` and `isyntax-backend`). You can specify as many paths as you like, as shown in line 2. For this walkthrough only the `/data` mount point is required.

?> On WSL2 use the Linux path (e.g. `/mnt/c/Users/<username>/Downloads/eats/data`) instead of the Windows path (e.g. `C:\Users\<username>\Downloads\eats`).

Let us take a look at the inputs for our app:

```bash
cd ./sample-apps/sample_apps/valid/tutorial/_01_simple_app/
code ./eats_inputs/my_wsi.json
cd -
```

```JSON
{
    "type": "wsi",
    "path": "/data/CMU-1.svs",
    "id": "37bd11b8-3995-4377-bf57-e718e797d515"
}
```

No need to edit anything here, just note that `"path"` contains our mount point. If more WSIs reside in `/SOME/PATH/TO/WSIS` or a subdirectory, they can be specified as well.

Start the services with:

```bash
eats services up ./wsi-mount-points.json
```

?> The very first run of the `eats` requires some minutes, as quite a few background services need to be build as docker images.

?> You can activate isyntax image file support by running `services up` with flag `--isyntax-sdk=<path/to/philips-pathologysdk-2.0-ubuntu18_04_py36_research.zip>` once. To get the SDK you have to register and download it from the Philips Pathology SDK Site (Note: Make sure to download the version for Ubuntu 18.04 and Python 3.6.9).

## Stop Services and Delete Jobs

It is possible to register and run multiple jobs without restarting backend services. The services can be stopped, if they are not needed anymore.

```bash
eats services down
```

To erase all data (registered WSIs, registered apps and jobs, etc.) use the `-v` switch:

```bash
eats services down -v
```
