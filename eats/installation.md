# Installation

Create a virtual environment and activate it (recommended but not required):

```bash
python3 -m venv .venv
source .venv/bin/activate
```

Install EATS from PyPI (Python Package Index):

```bash
pip install empaia-app-test-suite
```

?> On WSL 2 the `pip install` might take longer than usual, if the `.venv` is located in a Windows directory. The general performance of EATS 2.0.2 is not heavily impacted by this.

## Installation of Test Releases on Test PyPI

If you want you can install inofficial test releases from Test PyPI. Create virtual environment and activate it as shown above and change the `pip install` command (`-i https://test.pypi.org/simple/` tells `pip` to install the package from test.pypi.org, `--extra-index-url https://pypi.org/simple/` ensures that the dependencies are installed from pypi.org):

```bash
pip install -i https://test.pypi.org/simple/ --extra-index-url https://pypi.org/simple/ empaia-app-test-suite
```

## Installation up to EATS 2.0.1

Download the <a href="resources/empaia_app_test_suite-2.0.1.zip">empaia_app_test_suite-2.0.1.zip</a> and unzip the package:

```bash
unzip empaia_app_test_suite-2.0.1.zip -d eats
cd eats
```

?> On WSL 2 you can access your Windows Download directory using `cd /mnt/c/Users/<username>/Downloads`

Create a virtual environment and activate it (recommended but not required):

```bash
python3 -m venv .venv
source .venv/bin/activate
```

Install the app test suite:

```bash
pip install wheel
pip install ./dist/empaia_app_test_suite-2.0.1-py3-none-any.whl
```

?> On WSL 2 the `pip install` might take longer than usual, if the `.venv` is located in a Windows directory. The general performance of EATS 2.0 is not heavily impacted by this.
