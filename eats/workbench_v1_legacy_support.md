# Workbench v1 Legacy Support

As described in the previous sections [App with UI](/eats/apps_with_ui.md#) and [App without UI](/eats/apps_without_ui.md#) there is a difference in the creator of input data for jobs. These differences do not allow an app to be used correctly in the context of the WBC 1.0 and the WBC 2.0 at the same time.

We strongly recommend to use the **WBC 1.0** for **Apps without an APP UI** only. In the `eats jobs register` command use the `--v1` flag to set a static User ID as the creator of input data. On the other hand **never** set the parameter `--v1` if you want to use the **WBC 2.0** for **Apps with an App UI**. Omitting the flag will create and assign a Scope ID as the creator of data elements.

In future updates of the EMPAIA platform a Generic App UI will be available. This Generic App UI will provide similar functionalities to the WBC 1.0, but within the scope of the WBC 2.0 and Workbench v2 API. A soon as this alternative has been developed, the WBC 1.0 and the Workbench v1 API will be obsolete and the components will be removed from future EATS versions.
