# Publish

To publish your app you have to provide the following resources:

* App container image saved as `app-image.tar.gz` file
* EMPAIA App Description (EAD) as `ead.json` file
* Optional: app configuration parameters (defined in EAD) as `config.json` file
* Optional: App UI static resources (e.g. index.html) saved as `app-ui.zip` file

For testing purposes you should also provide anonymized example WSIs and inputs to the EMPAIA engineering team.

## Containerized App Example

This section will show, how an application could be wrapped into a docker container.

This example describes how to containerize a Python app. For other programming languages, e.g. C++, Java or .Net, the project structure and Dockerfile have to be adapted accordingly.

### Project structure

Assuming your applicaiton is written in python, you might have a `app.py`, with your apps main code, and some mechnism to specify needed packages and versions. For simplicity we chose a simple `requirements.txt` here. To containerize your app you need a `Dockerfile`. All three files are supposed to be in your apps root folder:

```
containerized_app_example/
  ├── app.py
  ├── requirements.txt
  └── Dockerfile
```

`app.py`:

For simplicity, and as you might already be familiar with that app, we take the [Tutorial Simple App](tutorial_backend/simple_app.md#) as example.

`requirements.txt`:

```
Pillow==7.2.0
requests==2.24.0
```

`Dockerfile`:

```Dockerfile
FROM python:3.6
ADD . /
RUN pip install -r requirements.txt
ENTRYPOINT python3 -u /app.py
```

Build the container image:

```bash
docker build -t sample-app .
```

Save the container image as a tarball:

```bash
docker save sample-app | gzip > app-image.tar.gz
```

## App UI Example

The App UI must be submitted as pre-built static files, not the raw source code of your JavaScript Framework. The following directory structure serves as an example:

```
app-ui/
├── assets/
│   └── fonts/
│       └── example-font.woff2
├── index.html
├── main.js
├── runtime.js
├── polyfills.js
└── styles.css
```

Zip only the contents of `app-ui`, such that the `app-ui` itself is not contained. The file should be named `app-ui.zip`.

## Testing

Test your app in the [EMPAIA App Test Suite](eats/eats.md#) before submitting the app.

## Submit your App

Contact [dev-support@empaia.org](mailto:dev-support@empaia.org) on how to submit your app. In a future update of the EMPAIA platform, an upload form will be provided.
