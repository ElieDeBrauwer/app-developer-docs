# Annotation Resolution

The following section covers the creation and retrieval of annotations in consideration of the resolution levels of a WSI. Due to the size and the level of detail of histopathological slides, a developer might want to affect the position where annotations are visible on the virtual image pyramid (i.e. on which resolution level an annotation is displayed). 

## Create Annotations

To address this issue we want to query annotations based on an npp (nanometer per pixel) range. In some cases it might make sense to display annotations only on a certain zoom level. For instance, if annotations label very small objects on the base layer or in the reverse case, if there is an annotation that is spanning over a large area on the tissue. For this case, the query parameters `npp_created` (required) and `npp_viewing` (optional) are introduced in the Workbench API. If an annotation should be displayed on a specific resolution level, it is sufficient to set the `npp_created` parameter. However, when it is desired to display annotations on multiple resolution levels of the WSI, it is required to set the optional `npp_viewing` parameter. To provide the right values for this field we give a short demonstration on how to calculate this value:

Let us assume we have a WSI with a magnification of 20x and thus a pixel size of approximately 500nm on the base layer. The image pyramid contains three levels with downsample factors of 1 (level 0), 4 (level 1) and 16 (level 2) (taken from our example in [WSI data](specs/wsi_data.md#)). Visualizing an annotation exclusively on the base layer is accomplished by setting `npp_viewing` to [0, 500]. If an annotation should only be visible between level 0 and level 1, it is calculated as follows:

```python
npp_lower = 1 * 500 # downsample_factor(level 0) * pixel_size_nm
npp_upper = 4 * 500 # downsample_factor(level 1) * pixel_size_nm
npp_viewing = [npp_lower, npp_upper]
```

## Render Annotations

In order to reduce the number of annotations that have to be queried and rendered by the Workbench Client, the query sets a filter option to only match annotations, that are located on the WSI level currently viewed.

The formula used by the viewer takes into account the current resolution value `c` (unit npp: nanometers per pixel) to calculate a lower boundary `a` and an upper boundary `b`. The resulting range `[a, b]` spans exactly one layer, assuming that the resolution from one layer to another differs by a factor of two.

```python
a = (c/2 + c)/2  # c*0.75
b = (c*2 + c)/2  # c*1.50
npp_query = [a, b]
```

As stated in the previous section, an app has to define an `npp_created` field. If the `npp_created` value lies between the upper and lower boundary, it is matched by the `npp_query` and rendered in the viewer.

If the app defines the optional `npp_viewing` field for an annotation, it can set a resolution range. In this case, an annotation will be matched if the slide-viewer's `npp_query` range and the annotation's `npp_viewing` range overlap. The annotation's `npp_created` value will be ignored if a `npp_viewing` range is set.

The annotation level of detail differs greatly between apps. To ensure not to overload the viewer with a large number of annotations only one layer is requested at a time. App developers can however decide to present certain annotations on multiple layers by setting `npp_viewing`, if the number of annotations is not excessive. This should be tested using the Workbench Client.

For every annotation, that is located on a higher resolution than matched by the query, a centroid (x, y coordinates) is returned. The annotation centroids are used to visualize these annotations in a cluster. Therefore the user knows where to find more annotations when zooming in.
