# App UI Requirements

## Web Security

The EMPAIA platform requires App UIs to comply with security Restrictions:

* do not load external fonts or icons via `<link>`
  * all resources must be part of the compiled / bundled App
* do not use other APIs except the Workbench v2 Scopes API at `/v2/scopes/`
* do not embed other web apps via iframe in your App UI
* do not access cookies, local storage or indexedDB
* do not access the DOM outside the iframe of your own App UI
* do not use lazy resource loading in your app

?>  Please contact us, if certain restrictions are problematic for your App UI functionality or developement workflow.

The rules mentioned above are enforced through technical measures.

1. The Frontend Token is only valid for one minute, therefore all static resources must be loaded immediately
1. The iframe is limited to functionalities available through `sandbox="allow-scripts"`
2. The Content-Security-Policy headers enforced by the Workbench v2 Frontends API prevent the access to external APIs and resources as follows:

```HTML
<meta http-equiv="Content-Security-Policy" content="default-src 'self' 'unsafe-inline'; style-src 'self' 'unsafe-inline'; img-src 'self' blob:">
```

?> `unsafe-inline` is a policy specifically required for Angular apps, enabling components to load their styles. If you encounter a similar issue with another framework, please contact us.
