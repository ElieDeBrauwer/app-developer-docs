# Workbench API

The Workbench v2 API provides general endpoints for the WBC 2.0 (`/v2`), provides access to frontend resources for WBC iframes (`/v2/frontends`) and allows App UIs to access medical data through the Workbench v2 Scopes API (`/v2/scopes`). This documentation only refers to the Scopes API (henceforth referred to as Workbench API), as it is the only part of the API relevant for app development.

Through the Workbench API an App UI can create and retrieve medical data in the EMPAIA Platform. For example an App UI may query WSIs and their image tiles to be rendered in a viewer. It also allows for running the associated app in the platform backend by assembling a job with certain input data (e.g. a ROI annotation drawn on a WSI). When the app processing is completed, the output data can be queried and displayed to the user. API access is **always** bound to a **Scope** to prevent accessing the data owned by other apps. A Scope has its own ID and is uniquely tied to the combination of examination ID, app ID and user ID. However, it is not tied to a certain browser session. Instead, if a user opens the same examination and app in another browser session, the same Scope ID is provided to the App UI. An examination is itself tied to a case, therefore only the data of the given case (e.g. list of slides) can be retrieved from a Scope. An App UI must always send a valid Scope ID and Scope Token to authorize requests.

Summary of functionality of the Workbench API:

* Retrieve slides and associated slide data valid for the selected case
* Post data as job inputs (e.g. annotations, classes, primitives, etc.)
* Create and execute jobs for the associated app
* Retrieve result data produced by jobs (e.g. annotations, classes, primitives, etc.)

## OpenAPI Specification

<a href="specs/wbs2_scopes_redoc" target="_blank">OpenAPI specification in Redoc</a>

For additional information on how to connect an App UI to the Workbench API, please refer to the [Tutorial: Frontend](tutorial_frontend/tutorial.md#) section.

The following sections list and explain certain key concepts of the Workbench API. These sections follow the same structure as the OpenAPI Specification.

## General Remarks

As described above, a Scope is the composition of an `examination_id`, an `app_id` and the `user_id`. To authorize against the Workbench API you will need a `scope_id` and have to set a valid `scope_token` in the request headers. This is required for every API-call.

```
Authorization: Bearer {scope_token}
```

!> The `scope_id` and `scope_token` **MUST** belong to the same Scope, otherwise your request will be rejected. For all posted data the `creator_id` **MUST** be the `scope_id` and `creator_type` **MUST** be `"scope"`

For information on how to retrieve the `scope_id` and `scope_token` see [Tutorial: Frontend](tutorial_frontend/tutorial.md#)

## Scope Panel

Via this endpoint all relevant Scope data can be retrieved (e.g., `examination_id`, `app_id`, `user_id`, and `ead`).

## Job Panel

The required workflow to successfully create and run jobs is as follows:

1. Create a new job (`POST`)
2. Create or select input data
   1. WSIs must be selected from an underlying Case
   2. All other inputs (ROIs / Annotations, Classes, Primitives, Collections) must be created (`POST` endpoints from [Data Panel](specs/workbench_api.md#data-panel))
3. Required app inputs declared in the EAD must be added to the job (`PUT /jobs/{job_id}/input/{input_key}`)
   1. `input_key` **MUST** match the input of the EAD
   2. Posted input type **MUST** match `type` declared in the EAD
4. When all required inputs are added, the job can be executed (`PUT /jobs/{job_id}/run`)
   1. All job inputs and class constraints declared in the EAD are validated
   2. All inputs are **locked** for the current `job_id` (the concept of **locking** is explained in [Locking](specs/workbench_api.md#locking))
   3. The job is scheduled for execution
5. Periodically request the job to check the current status
6. When job execution has finished
   1. Request all job data (inputs and / or results) via endpoints from [Data Panel](specs/workbench_api.md#locking)

!> Once a job is started it can't be modified or deleted.

### Jobs out of Scope

!> The endpoint `GET /scopes/{scope_id}/jobs-out-of-scope` is the only way to access jobs created in the Workbench Client 1.0 via an App UI. Your App UI **MUST NOT** depend on this endpoint because it could be removed in future releases. During the development process or the transition of an app **without** UI to an app **with** UI this endpoint could be convenient.

## Slide Panel

Via the `GET /slides` endpoint an App UI has access to all slides assigned to the underlying Case.

For the properties `tissue` and `stain` the information in `mappings` can be used to display translated clinical information for each slide.

For more information regarding slide data (metadata and image data) see [WSI Data](specs/wsi_data.md#)

## Data Panel

The endpoints in the Data Panel enable an App UI to create input data for jobs and to retrieve all required job data to visualize the results of jobs after completion.

### Data Creation

For information about the creation of data refer to the Data Panel `POST` endpoints of the <a href="specs/wbs2_scopes_redoc" target="_blank">OpenAPI specification</a>

!> All `POST` endpoints in the Data Panel have the query parameter `external_ids`. This parameter is used in the EATS for testing purposes. In **ALL** deployments of the EMPAIA Platform external IDs will **ALWAYS** be disabled! Your app **MUST NOT** depend on the possibility to post externally generated IDs! All IDs in posted data will be **IGNORED**!

#### Annotations and Class constraints

If an EAD defines a class constraint, a valid class must be created for each affected annotation. This can be done using the `POST /class` endpoint. An exception is the global class ROI (class value `org.empaia.global.v1.classes.roi`), that can not be created through the `POST /classes` endpoint. ROI classes **MUST** be created with the query parameter `is_roi=true` in the `POST` annotation endpoint. When doing so, for each annotation a class with the value `org.empaia.global.v1.classes.roi` and a correct reference is created.

#### Annotations and Centroids

Annotations can be created with an optional `centroid`. If no `centroid` is given, it will be calculated during `POST`.

A posted `centroid` will **NOT** be checked!

For more information about the function of centroids see [Annotation Queries](specs/workbench_api.md#annotation-queries).

#### Annotations and Resolution

Posted annotations **MUST** contain the property `npp_created` indicating the resolution in nanometers per pixel on which the annotation was created.

Annotations can also contain the property `npp_viewing`. For more information see [Annotation Resolution](specs/annotation_resolution.md#).

#### Collections and Collection Items

Collections can be created or extended in different ways:

1. Post a new collection with new items in a single request
2. Post a new empty collection and add new items in one or more separate requests using the `POST /collections/{collection_id}/items` endpoint
3. Post a new collection with previously created items in a single request
4. Post a new empty collection and add previously created items in one or more separate requests using the `POST /collections/{collection_id}/items` endpoint

!> The `item_type` of the collection and type of items posted together with the collection or added afterwards **MUST** always match!

If previously created items are posted (together with a collection or added after creation), each item **MUST** be formatted as `{"id": id}`, where `id` is the returned `id` of a previously created data element. New items and previously created items can not be mixed in a single request.

Items can always be added to collections no matter how the collection was created (empty or with items).

### Data Locking

Data Locking is a key concept to link all relevant data to a specific job. The process of locking is performed in two steps (only the first step is done by the App UI):

1. Input data is locked when an App UI calls the endpoint to execute a job
2. Output data is locked when an app calls the finalize endpoint

The Medical Data Service will persist a lock as a tuple of the item ID and job ID.

Locked data can be reused in the **SAME** Scope, e.g. a ROI can be input for multiple jobs.

Collections are locked recursively, i.e. all containing items are locked as well. Created classes that are defined as class contraints in the EAD are locked when the annotation, the class refers to, is locked.

!> Locked data can't be modified or deleted.

### Data Retrieval

For information about the retrieval of data refer to the Data Panel `GET` and `PUT /query` endpoints of the <a href="specs/wbs2_scopes_redoc" target="_blank">OpenAPI specification</a>

All `GET` and `PUT /query` endpoints can be used with the query parameters `skip` and `limit`. These parameters enable a batchwise retrieval of the data and thus provide the functionality to implement pagination for data visualization. All responses are sorted by creation time in descending order.

Using `skip` or `limit` doesn't influence the `item_count`. `item_count` always indicate the number of items that match the filter criteria.

#### Queries

Data can be queried by using the `PUT /query` endpoints. A query is semantically a filter to indicate what properties the returned data must have. The general form of all data queries is:

```JSON
{
   "creators": [...],
   "references": [...],
   "jobs": [...]
}
```

* `"creators"` is a list of `creator_id`s: filter data by creator (**MUST** be `scope_id`)
* `"references"` is a list of `reference_id`s: filter data by their reference (e.g. a single `slide_id` to retrieve all annotations for that slide)
* `"jobs"` is a list of `job_id`s: filter data by the jobs it was locked for (e.g. a single `job_id` to retrieve all data for that job; inputs **AND** outputs)

!> For `jobs` only `job_id`s within the current Scope are allowed!

A filter **MUST** contain at least one value. If you don't want to use a certain filter, the property **MUST** be ommitted. The values for one filter are logically connected by **OR**, the different filters by **AND**. Each additional filter will always maintain or reduce the number of matching items, never increase it.

For some data types additional filters are available or additional values are allowed!

#### Primitive Queries

Primitives don't require a reference, therefore `references` can contain `null` to include primitives without references.

#### Collection Queries

Collections queries allow an additional filter:

* `item_types` is a list of valid item types to filter collections by `item_type` (values can be: `"wsi"`, `"integer"`, `"float"`, `"bool"`, `"string"`, `"point"`, `"line"`, `"arrow"`, `"circle"`, `"rectangle"`, `"polygon"`, `"class"` or `"collection"`)

Collection queries also allow an additional query parameter:

* `shallow=true` return `items` of the most inner collections as an empty lists

#### Collection Items Queries

The items of collections can be queried as well. The available query filters are:

```JSON
{
   "references": [...],
   "viewport": {"x": int, "y": int, "width": int, "height": int},
   "npp_viewing": [int, int]
}
```

The filters `"viewport"` and `"npp_viewing"` are only considered if the item type of the collection is an annotation type (`"point"`, `"line"`, `"arrow"`, `"circle"`, `"rectangle"` or `"polygon"`) and ignored otherwise.

#### Class Queries

Class queries can be sent with the additional query parameter `with_unique_class_values=true` to include a list of all unique class values in the response.

The usage of `skip` or `limit` does not influence the values for `unique_class_values`. 

If you are only interested in the list of unique class values, you can set `limit=0`.

#### Annotation Queries

Annotation queries allow additional filters:

* `"types"` is a list of annotation types to filter annotations by type (e.g. `point` or `polygon`)
* `"viewport"` is a JSON dictionary (`{"x": int, "y": int, "width": int, "height": int}`) and filters annotations by location
  * Every annotation within the given region (incl. intersections) is returned
  * **REMARK:** An annotation is returned, if its bounding box overlaps with the viewport (bounding boxes for annotations are created and persisted automatically during a `POST`)
* `"npp_viewing"` is a tuple of lower and upper resolutions in npp (nanomater per pixel) to filter annotations by creation or viewing resolution (for more information see [Annotation Resolution](specs/annotation_resolution.md#))
* `"class_values"` is a list of class values to filter annotations by the values of classes referencing them
  * can contain `null` if annotations without classes should be included
* `"annotations"` is a list of annotation `id`s to filter annotations by ID (for an example why this could be useful see [Annotation Viewer Query](specs/workbench_api.md#annotation-viewer-query))

Annotation queries also allow additional query parameter:

* `with_classes=true` include classes referencing each returned annotation. The classes are included as a list for the property `classes`. If an annotation has no class, `classes` is an empty list
* `with_low_npp_centroids=true` includes a list of centroids in the response. The centroids can be used in a viewer to signal the user the presence of annotations that would become visible by zooming further into that region of the slide. The list contains all annotations as a centroid representation, that
  * match the filter criteria except `npp_viewing`
  * the range / values for `npp_viewing` or `npp_created` of the annotations are lower than the `npp_viewing` range from the query (for further information see [Annotation Resolution](specs/annotation_resolution.md#))

?> The query parameter `with_low_npp_centroids` can only be used if `"npp_viewing"` is used as a filter.

#### Annotation Viewer Query

The endpoint `PUT /annotations/query/viewer` is a special flavor of the general annotations query endpoint. The filters are equal (except `annotations`) and there are no query parameters. The response always contains IDs of all annotations matching the filter criteria and centroids. The returned IDs can then be used as filter for the general query. This procedure, which at first glance seems cumbersome, allows annotations to be cached by their IDs to only request new annotations in a second API-call. Depending on the number of annotations this could result in a significant performance improvement.

!> Both queries **MUST** use the same filters except `annotations` (only in the general annotations query) if you want to use the described method for annotation caching.

#### Annotation Count Query

A convenience endpoint to only retrieve the count of matching annotations.

The same effect can be achieved if `limit=0` is used in the general annotations query. The general query can be noticeably slower than the special count query.

#### Annotation Unique Class Values Query

This endpoint returns a list of unique class values for annotations matching filter criteria.

Result may contain `null` if annotations without assigned classes matching the filter criteria.

#### Annotation Query Position

Assuming you visualize all annotations for a slide in a slide viewer and a paginated subset of n related slide items in a list beside the viewer. If the user clicks on an annotation in the viewer the list should update to show the relevant subset that includes the clicked annotation. With the annotation query position endpoint you can retrieve the position of an annotation in the response property `items` list of a general annotations query. With the position you are able to set `skip` and `limit` accordingly and retrieve the relevant subset to update your list.

!> The filter for all involved queries **MUST** be identical.

#### Unique References Queries

These endpoints return all unique reference IDs of the items matching the filter criteria. The IDs are grouped by the reference types `annotation`, `collection` and `wsi`. Collections and Primitives include the optional property `contains_items_without_reference` to indicate if there are matching items without a reference.

### Modify and Delete Data

Only data that is **NOT** locked can be modified or deleted.

If a collection is deleted, the items are **NOT** deleted recursively

!> All **Inputs** and **Outputs** of started Jobs are **immutable**.
