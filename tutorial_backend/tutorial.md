# Tutorial: Backend

This tutorial covers the EMPAIA App Description (EAD) format and the App API, that provides endpoints to retrieve and send data as specified in the EAD. The tutorial starts with a very simple app and how it can be described in EAD. During the course of this guide further features are introduced step-by-step.

All scenarios mentioned in this tutorial can be found here:

[Sample Apps Repository](https://gitlab.com/empaia/integration/sample-apps/-/tree/master/sample_apps/valid/tutorial)

?> Code examples in this tutorial are written in Python, but all HTTP service calls can be translated to other programming languages.

?> For detailed information about the EAD format and App API, see [Specs](specs/specs.md#).
