# Sample App Backend

?> Before starting with the tutorial of the App UI development, please take a look at the app the UI will be based on. If you have worked through the examples in the [Backend Tutorial](turorial_backend/tutorial.md#) no new concepts will be shown here.

The EAD for the sample app specifies:

* Two Classes
* Single WSI as input
* Single rectangle annotation as input
* Collection of point annotations as output
* Collection of floats as output
* Collection of classes as output
* Two single integers as output
* Single float as output

## Inputs

* 1 WSI (key: slide).
* 1 Rectangle annotation (key: region_of_interest) specifying the region to be processed.
  * must have class `org.empaia.global.v1.classes.roi`

## Outputs

* 1 Point annotation collection (key: detected_nuclei), where each result annotation references the input WSI.
* 1 Float primitive collection (key: model_confidences), where each result confidence references one of the point annotations.
* 1 Class collection (key: nucleus_classifications), where each result class references one of the point annotations.
  * Values for the classes must be declared in `"classes"` within the EAD
* 1 Integer primitive (key: number_positive), references the input rectangle.
* 1 Integer primitive (key: number_negative), references the input rectangle.
* 1 Float primitive (key: positivity), references the input rectangle.
  
## EMPAIA-App-Description

```JSON
{
    "$schema": "https://developer.empaia.org/schema/ead-app-schema-draft-3.json",
    "name": "EMPAIA Sample Frontend App 01",
    "name_short": "SFEA01",
    "namespace": "org.empaia.vendor.sample_frontend_app_01.v1",
    "description": "Sample app to demonstrate custom frontend components in the WBC 2.0",
    "classes":{
        "pos":{
            "name":"Positive"
        },
        "neg":{
            "name":"Negative"
        }
    },
    "inputs": {
        "slide": {
            "type": "wsi"
        },
        "region_of_interest": {
            "type": "rectangle",
            "reference": "inputs.slide",
            "classes": [
                "org.empaia.global.v1.classes.roi"
            ]
        }
    },
    "outputs": {
        "detected_nuclei": {
            "type": "collection",
            "reference": "inputs.region_of_interest",
            "items": {
                "type": "point",
                "reference": "inputs.slide"
            }
        },
        "model_confidences": {
            "type": "collection",
            "items": {
                "type": "float",
                "reference": "outputs.detected_nuclei.items"
            }
        },
        "nucleus_classifications": {
            "type": "collection",
            "items" : {
                "type": "class",
                "reference": "outputs.detected_nuclei.items"
            }
        },
        "number_positive": {
            "type": "integer",
            "reference": "inputs.region_of_interest"
        },
        "number_negative": {
            "type": "integer",
            "reference": "inputs.region_of_interest"
        },
        "positivity":{
            "type": "float",
            "reference": "inputs.region_of_interest"
        }
    }
}
```
