# Tutorial: Frontend

This tutorial covers the development of app specific App UIs using the Workbench API, that provides endpoints to retrieve and send data to visualize all app related data. This includes the creation of jobs with inputs as specified in the EAD. The tutorial starts with a brief description of the backend part of the [Sample App](tutorial_frontned/sample_app_backend). The App UI described in the subsequent sections is customized for this specific app.

The source code of the software components described in the tutorial is available on GitLab:

- [Sample App (Backend)](https://gitlab.com/empaia/integration/sample-apps/-/tree/master/sample_apps/valid/with_frontend/sample_frontend_app_01)
- [Sample App UI](https://gitlab.com/empaia/integration/frontend-workspace/-/tree/master/apps/sample-app)

?> Code examples in this tutorial are written in Python for backend code and JavaScript / TypeScript using Angular v13 for frontend code. Note, that you are free to use any other programming languages or frameworks for your implementation.

For detailed information about the EAD and the backend examples, see [Tutorial: Backend](tutorial_backend/tutorial.md#). For further information about the Workbench API, see [Workbench API](specs/workbench_api.md#). Also take a look at the [App UI Requirements](specs/app_ui_requirements.md#) specification.
